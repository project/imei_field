IMEI Field module adds a new field to your site so you can attach an IMEI 
field to any of your fieldable entities.
IMEI stands for International Mobile Station Equipment Identity 
(http://en.wikipedia.org/wiki/Imei), and is used to uniquely identify mobile 
devices across the globe.

IMEI field module's field will make sure that the IMEI number entered is 
correct, and it will also apply clean up when possible.
A valid IMEI should
 - Consist of only numbers.
 - 15 digits in length.
 - Should pass Luhn Algorithm test (http://en.wikipedia.org/wiki/Luhn)


To add an IMEI field, go to the appropriate "Manage Fields" tab, and choose 
"IMEI" from the "Field type" list. You can configure the width of the field, 
number of fields, field title, whether field is required, and almost anything
under default field settings.

To view the IMEI number of a mobile device, dial *#06# from the call pad/screen.
